package employee.employeemanagement.models.maps;

import employee.employeemanagement.models.domain.Employee;
import employee.employeemanagement.models.dto.EmployeeDevicesSummaryDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class EmployeeDevicesSummaryDtoMapper {
    /**Return a map containing employee's full-name and numbers of devices*/
    public static EmployeeDevicesSummaryDto employeeDevices(List<Employee> employees){
        Map<String, Integer> employeeDevicesMap = new HashMap<>();

        for (Employee employee: employees) {
            var fullName = employee.getFirstName() + " " + employee.getLastName();
            var devices = employee.getDevices().size();
            employeeDevicesMap.put(fullName,devices);
        }
        return new EmployeeDevicesSummaryDto(employeeDevicesMap);
    }
}
