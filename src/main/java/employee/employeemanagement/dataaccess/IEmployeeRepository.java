package employee.employeemanagement.dataaccess;

import employee.employeemanagement.models.domain.Employee;
import employee.employeemanagement.models.dto.EmployeeDevicesSummaryDto;
import employee.employeemanagement.models.dto.FullTimeEmployeesDto;
import employee.employeemanagement.models.dto.HiredEmployeeDto;
import java.util.ArrayList;
import java.util.List;

public interface IEmployeeRepository {
    /**Return all the employees from the list*/
    List<Employee> getAllEmployees();
    /**Add one employee to the employees' list according to the body given*/
    Employee addEmployee(Employee employee);
    /**Return one employee according to the ID given*/
    Employee getEmployee(int id);
    /**Replace the employee according to the ID and the employee body given*/
    Employee replaceEmployee(int id, Employee employee);
    /**Modifies the employee according to the ID and body given*/
    Employee modifyEmployee(int id, Employee employee);
    /**Check if there is any employee in the employees' list*/
    boolean isEmployeesEmpty();
    /**Check is employee exists in the list*/
    boolean employeeExists(int id);
    /**Check if the Employee's body is valid*/
    boolean isEmployeeValid(Employee employee);
    /**Delete employee from the list according to the ID*/
    void deleteEmployee (int id);
    /**Remove the employee's device choosing an ID and writing device's name*/
    Employee removeDevice(int id, String deviceName);
    /**Add one device to the employee choosing an ID and writing device's name */
    Employee addDevice(int id, String deviceName);
    /**Get the amount of devices each employee have*/
    EmployeeDevicesSummaryDto employeesNumberOfDevices();
    /**Get the employees hired in the last 2 years */
    ArrayList<HiredEmployeeDto> employeesHired();
    /**Get the all full-time employees with date of birth and remaining days to the next birthday*/
    ArrayList<FullTimeEmployeesDto> fullTimeEmployees();
    /**Check if the luhn code already exists in the list*/
    boolean codeExists(String employeeCode);
}
