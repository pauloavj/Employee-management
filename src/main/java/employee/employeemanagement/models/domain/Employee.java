package employee.employeemanagement.models.domain;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
public class Employee {
    private static AtomicInteger count = new AtomicInteger(0);
    private int id;
    private String firstName;
    private String lastName;
    private String employeeCode;
    private LocalDate dateOfBirth;
    private WorkTime workTime;
    private Position position;
    private LocalDate dateHired;
    private ArrayList<String> devices;


    public Employee(String firstName,
                    String lastName,
                    String employeeCode,
                    LocalDate dateOfBirth,
                    WorkTime workTime,
                    Position position,
                    LocalDate dateHired,
                    ArrayList<String> devices) {
        this.id = count.incrementAndGet();
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeCode = employeeCode;
        this.dateOfBirth = dateOfBirth;
        this.workTime = workTime;
        this.position = position;
        this.dateHired = dateHired;
        this.devices = devices;
    }
    /**Reset the auto increment ID*/
    public void reset(){
        count.set(0);
    }
}
