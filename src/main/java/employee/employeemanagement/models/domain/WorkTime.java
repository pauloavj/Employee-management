package employee.employeemanagement.models.domain;

/**Part-time or Full-time employee*/
public enum WorkTime {
    PartTime,
    FullTime
}
