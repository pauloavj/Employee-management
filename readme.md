# Employee Management Application

This application is used to keep track of employees and their devices.


## Functions

**Through postman or other similar application the following requests can be done.**

1. Send a GET request with the link below to get all employees in the application.
```bash
https://employee-management-teste.herokuapp.com/employees
```
```bash
http://localhost:8080/employees/
```
2. Send a GET request with the link below replacing the argument id,
to get the specific employee.
```bash
https://employee-management-teste.herokuapp.com/employees/{id}
```
```bash
http://localhost:8080/employees/{id}
```
3. Send a POST request with the link below to create an employee,
use the body as specified below.

**Note:** The employee code must be a luhn number with 10 digits for the employee to be accepted
you can generate a number at https://www.dcode.fr/luhn-algorithm
```bash
https://employee-management-teste.herokuapp.com/employees
```
```bash
http://localhost:8080/employees
```
```bash
{
    "firstName": "Roger",
    "lastName": "Federer",
    "employeeCode": "8072635082",
    "dateOfBirth": "1980-02-14",
    "workTime": "PartTime",
    "position": "Tester",
    "dateHired": "2021-03-31",
    "devices": [
        "Dell XPS 13"
    ]
}
```
4. Send a PUT request with the link below to replace an employee, 
use the body template as specified below and replace the id between curly braces.

```bash
https://employee-management-teste.herokuapp.com/employees/{id}
```
```bash
http://localhost:8080/employees/{id}
```
```bash
{
    "firstName": "Roger",
    "lastName": "Federer",
    "employeeCode": "8072635082",
    "dateOfBirth": "1980-02-14",
    "workTime": "PartTime",
    "position": "Tester",
    "dateHired": "2021-03-31",
    "devices": [
        "Dell XPS 13"
    ]
}
```
5. Send a PATCH request with the link below to modify an employee, use 
the body template as specified below and replace the id between curly braces

```bash
https://employee-management-teste.herokuapp.com/employees/{id}
```
```bash
http://localhost:8080/employees/{id}
```
```bash
{
    "firstName": "Roger",
    "lastName": "Federer",
    "employeeCode": "8072635082",
    "dateOfBirth": "1980-02-14",
    "workTime": "PartTime",
    "position": "Tester",
    "dateHired": "2021-03-31",
    "devices": [
        "Dell XPS 13"
    ]
}
```
6. Send a DELETE request with the link below to remove an employee from the employee's list,
replace the id in curly braces.

```bash
https://employee-management-teste.herokuapp.com/employees/{id}
```
```bash
http://localhost:8080/employees/{id}
```

7. Send a PATCH request with the link below to remove a device from an employee, 
specify the employee id in the link below.
To remove the device it is necessary send a body with a String as the in example below. 

```bash
https://employee-management-teste.herokuapp.com/employees/removeDevice/{id}
```
```bash
http://localhost:8080/employees/removeDevice/{id}
```
```bash
Samsung 27-inch LED Monitor
```
8. Send a PATCH request with the link below to add a device to an employee,
   specify the employee id in the link below.
   To add the device it is necessary send a body with a String as the in example below.

```bash
https://employee-management-teste.herokuapp.com/employees/addDevice/{id}
```
```bash
http://localhost:8080/employees/addDevice/{id}
```
```bash
Samsung 27-inch LED Monitor
```
9. Send a GET request with the link below 
to get a summary from the number of devices for each employee 

```bash
https://employee-management-teste.herokuapp.com/employees/devices/summary
```
```bash
http://localhost:8080/employees/devices/summary
```
10. Send a GET request with the link below
   to get all the new employees with less than 2 years.

```bash
https://employee-management-teste.herokuapp.com/employees/new
```
```bash
http://localhost:8080/employees/new
```

11. Send a GET request with the link below
    to get all the full-time employees' birthday.

```bash
https://employee-management-teste.herokuapp.com/employees/birthdays
```
```bash
http://localhost:8080/employees/birthdays
```
