package employee.employeemanagement.models.domain;

/**Employee working position*/
public enum Position {
    Developer,
    Tester,
    Manager
}
