package employee.employeemanagement.models.domain;

import org.springframework.stereotype.Component;
import java.util.Arrays;

@Component
public class CheckEmployeeCode {
    /**Check if the employee 10 digits code follow the luhn algorithm*/
    public boolean isLuhnNumber(String number){

        int[] arrayNumber = new int[number.length()];

        for (int i = 0; i < number.length(); i++) {
            arrayNumber[i] = Integer.parseInt(String.valueOf(number.charAt(i)));
        }

        for (int i = 0; i < arrayNumber.length - 1; i+=2) {
            arrayNumber[i] = arrayNumber[i] * 2;
            if (arrayNumber[i] > 9){
                arrayNumber[i] = arrayNumber[i] % 10 + 1;
            }
        }
        return Arrays.stream(arrayNumber).sum() % 10 == 0;
    }
}
