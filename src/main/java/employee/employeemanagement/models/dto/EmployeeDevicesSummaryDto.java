package employee.employeemanagement.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@AllArgsConstructor
@Getter
@Setter
public class EmployeeDevicesSummaryDto {
    private Map<String, Integer> employeeDevicesSummary;
}
